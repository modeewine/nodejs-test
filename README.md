# NodeJS Test API

## Requirements

* Node 8.16.1+
* NPM 6.4.1+
* MySQL 5.7.28+

## Installation

1. Clone or place the `NodeJS Test API` to work directory.

    ```bash
    your/some$ cd test
    your/some/test$ git clone git@gitlab.com:modeewine/nodejs-test.git ./
    ```
   
2. Install all node dependencies.

    ```bash
    your/some/test$ npm install
    ```
   
3. SQL-dump (test.sql) file locates in the root directory which you have to execute in your existed (mySQL) database.   
   
4. Rename or copy `.env.dist` to `.env` and setup values.
   
## Run

1. Execute it and enjoy :)

    ```bash
    npm start
    ```

## Build using docker
Run `docker-compose up` to build the project using Docker. 
Warning! You need to install docker on your machine before deploy