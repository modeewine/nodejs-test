-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `access_token`;
CREATE TABLE `access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `expires_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `access_token` (`id`, `token`, `expires_at`) VALUES
(1,	'c0169af324093cfd96120be062ef6c12',	1580601600),
(2,	'c0159af324093cfd96120be062ef6c12',	1480601600);

DROP TABLE IF EXISTS `todo_list`;
CREATE TABLE `todo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `todo` varchar(255) DEFAULT NULL,
  `desc` longtext,
  `info` longtext,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `todo_list` (`id`, `todo`, `desc`, `info`, `completed`, `state`) VALUES
(1,	'test todo 1',	'description 1',	'info 1',	1,	'test 1'),
(2,	'test todo 2',	'description 2',	'info 2',	1,	'test 2');

-- 2019-12-28 09:14:23
