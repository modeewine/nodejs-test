const express = require('express');
const todo = require('./../services/todo');

const router = express.Router();

router.put('/todo/:id/update/:status', async (req, res, next) => {
    if (!req.params.id.match(/^\d+$/g)
        || !req.params.status.match(/^(completed|uncompleted)$/g)
    ) {
        res.status(400).json({message: 'Bad request'});
    } else {
        next();
    }
}, async (req, res, next) => {
    await todo.setCompleted(req.params.id, (req.params.status === 'completed'));
    res.json(await todo.getItem(req.params.id));
});

module.exports = router;
