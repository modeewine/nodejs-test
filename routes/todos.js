const express = require('express');
const todo = require('./../services/todo');

const router = express.Router();

router.get('/todos', async (req, res, next) => {
    res.json(await todo.getList());
});

module.exports = router;
