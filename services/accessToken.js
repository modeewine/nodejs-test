const db = require('./mysql.js');

module.exports = {
    check: async (token) => {
        let result = await db.query("SELECT * FROM access_token WHERE token = ? AND expires_at > UNIX_TIMESTAMP()", [token]);
        return !!(result.length);
    }
};
