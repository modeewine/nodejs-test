const mysql = require('mysql');

require('dotenv').config();

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
});

connection.connect(err => {
    if (err) throw err;
});

module.exports = {
    query: (query, params) => new Promise((resolve, reject) => {
        connection.query(query, params, (err, res) => {
            if (err) reject(err);
            if (res) resolve(res);
        });
    })
};
