const db = require('./mysql.js');

const tableTodoList = 'todo_list';

module.exports = {
    getList: async () => {
        return await db.query("SELECT * FROM " + tableTodoList, []);
    },
    getItem: async (id) => {
        let result = await db.query("SELECT * FROM " + tableTodoList + " WHERE id = ?", [id]);
        return (result.length ? result[0] : []);
    },
    setCompleted: async (id, status) => {
        return await db.query("UPDATE " + tableTodoList + " SET completed = ? WHERE id = ?", [status, id]);
    }
};
